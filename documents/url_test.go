package documents

import (
	"testing"

	"bitbucket.org/tomtom1967/goproductive/filesystem"
)

func TestURLFactory(t *testing.T) {
	fs, _ := filesystem.NewFactory().BuildMemory(map[string]string{})
	list := NewURLFactory(fs).BuildList("main")
	if len(list.List) != 0 {
		t.Errorf("Expected URLList to be 0, got: %d", len(list.List))
	}
	list.Add(NewURLFactory(fs).BuildURL("vouwwagen,camperen::http://www.3dogcamping.eu/nl/trailer.php"))
	if len(list.List) != 1 {
		t.Errorf("Expected URLList to be 1, got: %d", len(list.List))
	}
	list.Save()

	list = NewURLFactory(fs).BuildList("main")
	if len(list.List) != 1 {
		t.Errorf("Expected URLList to be 1, got: %d", len(list.List))
	}

	list.Add(NewURLFactory(fs).BuildURL("development,docker::https://www.digitalocean.com"))
	if len(list.List) != 2 {
		t.Errorf("Expected URLList to be 2, got: %d", len(list.List))
	}
	list.Save()
	list = NewURLFactory(fs).BuildList("main")
	if len(list.List) != 2 {
		t.Errorf("Expected URLList to be 2, got: %d", len(list.List))
	}

	found := list.Search("docker")
	if len(found.List) != 1 {
		t.Errorf("Expected to find 1, got: %d", len(found.List))
	}
}
