package documents

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"bitbucket.org/tomtom1967/goproductive/filesystem"
)

func TestJournalFactory(t *testing.T) {
	fs, _ := filesystem.NewFactory().BuildMemory(map[string]string{})

	j := NewJournalFactory(fs).BuildNow("main")
	if j.Name != "Journal"+time.Now().Format("20060102")+".md" {
		t.Fail()
	}

	j, _ = NewJournalFactory(fs).BuildDate("20191017", "main")
	if j.Name != "Journal20191017.md" {
		t.Errorf("Expected name Journal20191017.md. got: %s", j.Name)
	}

	j2, err := NewJournalFactory(fs).BuildFromFileName(j.Name, "sport")
	if err != nil {
		t.Error(err)
	}
	if j2.Name != "Journal20191017.md" {
		t.Errorf("Expected name Journal20191017.md. got: %s", j2.Name)
	}
}

func TestJournalSave(t *testing.T) {
	fs, _ := filesystem.NewFactory().BuildMemory(map[string]string{})
	j := NewJournalFactory(fs).BuildNow("main")
	j.Save("Dit is een Test")
	j.Save(fmt.Sprintf("Meer test%s", j.String()))
	if strings.Contains(j.String(), "Dit is een Test") == false {
		t.Errorf("Expexted text not found")
	}
	j.Save(fmt.Sprintf("Meer test%s", j.String()))
	if strings.Contains(j.String(), "Dit is een Test") == false {
		t.Errorf("Expexted text not found")
	}
	if strings.Contains(j.String(), "Meer test") == false {
		t.Errorf("Expexted text not found")
	}
	files := fs.Files("main")
	if len(files) > 1 {
		t.Errorf("Expexted len 1 got: %d", len(files))
	}
}

func TestJournalList(t *testing.T) {
	fs, _ := filesystem.NewFactory().BuildMemory(map[string]string{})
	jf := NewJournalFactory(fs)

	jl, err := jf.BuildJournalList("main")
	if err != nil {
		t.Error(err)
	}
	if len(jl) != 0 {
		t.Errorf("Expected len 0 got %d", len(jl))
	}

	// Add an Journal
	j, err := jf.BuildDate("20191017", "main")
	if err != nil {
		t.Error(err)
	}
	j.Save("Dit is een test")

	jl, err = jf.BuildJournalList("main")
	if err != nil {
		t.Error(err)
	}
	if len(jl) != 1 {
		t.Errorf("Expected len 1 got %d", len(jl))
	}

	// Add another Journal
	j, err = jf.BuildDate("20191016", "main")
	if err != nil {
		t.Error(err)
	}
	j.Save("Test 2")

	// Add a non journal file
	fs.Write("main/test.txt", []byte("TEST"))

	jl, err = jf.BuildJournalList("main")
	if err != nil {
		t.Error(err)
	}
	if len(jl) != 2 {
		t.Errorf("Expected len 2 got %d", len(jl))
	}

	for _, j := range jl {
		fmt.Println(j)
	}

}

func TestGetPreviousNextJournal(t *testing.T) {
	fs, _ := filesystem.NewFactory().BuildMemory(map[string]string{})
	jf := NewJournalFactory(fs)

	j1, _ := jf.BuildDate("20191016", "main")
	j1.Save("Johannes")
	j2, _ := jf.BuildDate("20191017", "main")
	j2.Save("Peter")
	j3, _ := jf.BuildDate("20191018", "main")
	j3.Save("Peter Freek")
	jl, _ := jf.BuildJournalList("main")

	// Test for error on wrong parameter
	_, err := jl.GetPreviousNextJournal(nil, true)
	if err == nil {
		t.Errorf("Expected a error for not giving a journal")
	}

	// Test to find the next journal j2 after j1
	next, err := jl.GetPreviousNextJournal(j1, true)
	if next == nil {
		t.Error("Expexted j2")
	}
	if next.ShortName() != j2.ShortName() {
		t.Error("Expected to find the journal 2")
	}

	// Test that there is no next after the last journal
	next, err = jl.GetPreviousNextJournal(j3, true)
	if next != nil {
		t.Error("Expexted that there is no next journal")
	}

	// Test that there is no provious before the first journal
	prev, err := jl.GetPreviousNextJournal(j1, false)
	if prev != nil {
		t.Error("Expexted that there should be no prev")
	}

	prev, err = jl.GetPreviousNextJournal(j2, false)
	if prev == nil {
		t.Error("Expexted to get the journal before j2")
	}
	if prev.ShortName() != j1.ShortName() {
		t.Error("Expected to find j1")
	}
}

func TestJournalListSortOrder(t *testing.T) {
	fs, _ := filesystem.NewFactory().BuildMemory(map[string]string{})
	jf := NewJournalFactory(fs)

	fs.Write("test.txt", []byte("TEST"))

	j1, _ := jf.BuildDate("20191017", "main")
	j1.Save("Peter")
	j2, _ := jf.BuildDate("20191018", "main")
	j2.Save("Peter Freek")
	j3, _ := jf.BuildDate("20191016", "main")
	j3.Save("Johannes")

	jl, _ := jf.BuildJournalList("main")
	if jl[0].Name != "Journal20191018.md" {
		t.Errorf("Expected: Journal20191018.md, got : %s", jl[0].Name)
	}
	if jl[1].Name != "Journal20191017.md" {
		t.Errorf("Expected: Journal20191017.md, got : %s", jl[0].Name)
	}
	if jl[2].Name != "Journal20191016.md" {
		t.Errorf("Expected: Journal20191016.md, got : %s", jl[0].Name)
	}

}

func TestJournalIndexAdd(t *testing.T) {
	jl, err := getJournalTestList()
	if err != nil {
		t.Error(err)
	}
	// This is where we test the index.Add method
	ji := jl.CreateJournalIndex()
	//ji := jl.CreateJournalIndex()
	found := ji.FindAny("Peter")
	if len(found) != 2 {
		t.Errorf("Expected 2 Peter found journals, found: %d", len(found))
	}
	found = ji.FindAny("Freek")
	if len(found) != 1 {
		t.Errorf("Expected 1 Freek found journals, found: %d", len(found))
	}
	found = ji.FindAny("Freek", "Johannes")
	if len(found) != 2 {
		t.Errorf("Expected 2 Freek and Johannes, found journals, found: %d", len(found))
	}
}

func getJournalTestList() (JournalList, error) {
	fs, _ := filesystem.NewFactory().BuildMemory(map[string]string{})
	jf := NewJournalFactory(fs)

	j1, _ := jf.BuildDate("20191016", "main")
	j1.Save("aan Johannes zelf Zal")
	j2, _ := jf.BuildDate("20191017", "main")
	j2.Save("aan Peter zelf Zal")
	j3, _ := jf.BuildDate("20191018", "main")
	j3.Save("aan Peter Freek zelf Zal")
	return jf.BuildJournalList("main")
}
