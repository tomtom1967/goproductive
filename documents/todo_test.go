package documents

import (
	"testing"

	"bitbucket.org/tomtom1967/goproductive/filesystem"
)

func TestFactory(t *testing.T) {
	fs, _ := filesystem.NewFactory().BuildMemory(nil)
	td := NewTodoFactory(fs).New("main")

	if len(td.Byte()) != 0 {
		t.Errorf("Expected empty content")
	}
	content := []byte("# Dit is een test")
	td.Save(content)
	if len(td.Byte()) != len(content) {
		t.Errorf("Expected content to be te same")
	}

	html := td.RenderToHTML()
	if html != "<h1>Dit is een test</h1>\n" {
		t.Errorf("Expected <h1>Dit is een test</h1>, got: %s", html)
	}
}
