package documents

import (
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"strings"

	"bitbucket.org/tomtom1967/goproductive/filesystem"
)

const urlsFile = "/urls.txt"

// URLlist is a List of type URL
type URLlist struct {
	filesystem filesystem.Filesystem
	List       []URL
	section    string
}

// URL structure containing the url and tags
type URL struct {
	URL  string
	Tags []string
	Hash string
}

// URLFactory creates urlList and URL
type URLFactory struct {
	filesystem filesystem.Filesystem
}

// NewURLFactory returns a pointer to the URLFactory ready for action
func NewURLFactory(fs filesystem.Filesystem) *URLFactory {
	return &URLFactory{fs}
}

// BuildList creates a list of url's from the urls.txt
func (f *URLFactory) BuildList(section string) *URLlist {
	content := string(f.filesystem.Read(section + urlsFile))
	list := URLlist{
		section:    section,
		filesystem: f.filesystem,
	}
	lines := strings.Split(content, "\n")
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		list.List = append(list.List, f.BuildURL(line))
	}
	return &list
}

// BuildURL creates a URL from a line of the urls.txt
func (f *URLFactory) BuildURL(line string) URL {
	if line == "" {
		return URL{"http://", []string{"-"}, ""}
	}
	part := strings.Split(line, "::")

	hasher := sha1.New()
	hasher.Write([]byte(part[1]))
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))

	return URL{part[1], strings.Split(part[0], ","), sha}
}

// Add adds a url to the list and saves it
func (list *URLlist) Add(url URL) {
	list.List = append(list.List, url)
	//list.Save()
}

// Save saves the list to filesystem
func (list *URLlist) Save() {
	content := ""
	for _, url := range list.List {
		content += url.String()
	}
	//fmt.Println(content)

	list.filesystem.Write(list.section+urlsFile, []byte(content))
}

// Search trys to find a tage
func (list URLlist) Search(tag string) URLlist {
	return list.search(tag)
}

func (list URLlist) search(tag string) URLlist {
	out := URLlist{}
	for _, url := range list.List {
		if url.hasTag(tag) {
			out.List = append(out.List, url)
		}
	}
	return out
}

// Delete deletes a url from the list
func (list URLlist) Delete(hash string) {
	content := ""
	for _, u := range list.List {
		if hash == u.Hash {
			continue
		}
		content += u.String()
	}
	list.filesystem.Write(list.section+urlsFile, []byte(content))
}

// hasTag finds string in tags
func (u URL) hasTag(tag string) bool {
	for _, v := range u.Tags {
		if strings.Contains(v, tag) {
			return true
		}
	}
	return false
}

// ShortenURL truncates the url to length characters
func (u URL) ShortenURL(length int) string {
	return u.shortenURL(length)
}

func (u URL) shortenURL(length int) string {
	characters := len(u.URL)
	if characters > length {
		characters = length
	}
	url := []byte(u.URL)

	return string(url[:characters])
}

// String makes a string from the url
func (u URL) String() string {
	return u.string()
}

func (u URL) string() string {
	tags := ""
	for _, t := range u.Tags {
		tags += t + ","
	}
	tags = strings.Trim(tags, ",")

	return fmt.Sprintf("%s::%s\n", tags, u.URL)
}

// Byte Returns the url as a []byte
func (u URL) Byte() []byte {
	return []byte(u.String())
}

// StripSchema returns the url without the schema
func (u URL) StripSchema() string {
	return u.stripSchema()
}

// StripSchema returns the url without the schema
func (u URL) stripSchema() string {
	s := strings.Replace(u.URL, "https://", "", 1)
	return strings.Replace(s, "http://", "", 1)
}
