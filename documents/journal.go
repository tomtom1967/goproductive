package documents

import (
	"fmt"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/tomtom1967/goproductive/filesystem"
	"gitlab.com/golang-commonmark/markdown"
)

// JournalIndex contains al the words of the journals and points to the journals these words ar in.
// This structure can be used to search for a word in the journals
type JournalIndex map[string][]*Journal

// FindAny finds any journal in the index
func (ji JournalIndex) FindAny(ss ...string) JournalList {
	return ji.findAny(ss)
}

func (ji JournalIndex) findAny(ss []string) JournalList {
	found := []*Journal{}
	for _, s := range ss {
		if v, ok := ji[strings.ToLower(s)]; ok {
			found = append(found, v...)
		}
	}
	return found
}

// Add add's a journal to the index
// Adding a journal this way adds a extra entry in the index of the existing document.
// This gives me two positive matches of the same journal
func (ji JournalIndex) Add(j *Journal) JournalIndex {
	return ji.add(j)
}

func (ji JournalIndex) add(j *Journal) JournalIndex {

	// Lastig om een journal toe te voegen.
	// Want, als de journal al bestaat dan wil je hem er uit halen en weer opnieuw invoeren.
	// Maar door de organisatie van de index "[woord]=[journaals]" moet je over ieder woord lopen en kijken of de Journal er al in zit.
	// Dan moet je journaal ontkoppelen van het woord. Maar dan kan het voorkomen dat een woord een lege journaal lijst heeft.
	// Daarbij schaalt deze constructie denk ik niet om dus snel een journaal toe te voegen.
	// Maar wellicht beter dan de index steeds helemaal opnieuw op te bouwen . . .
	// VERBETER PUNTJE DUS.
	// Zoeken op index algoritme's

	// removing the new lines
	re1 := regexp.MustCompile("\\n+")
	txt := re1.ReplaceAllString(j.String(), " ")
	// removing non word strings
	re2 := regexp.MustCompile("\\W+")
	txt = re2.ReplaceAllString(txt, " ")

	txt = strings.ToLower(txt)
	// removing all dubble spaces
	re3 := regexp.MustCompile(" +")
	txt = re3.ReplaceAllString(txt, " ")

	// Regex for removing stop words
	re4 := regexp.MustCompile("\\A(aan|aangaande|aangezien|achte|achter|achterna|af|afgelopen|al|aldaar|aldus|alhoewel|alias|alle|allebei|alleen|alles|als|alsnog|altijd|altoos|ander|andere|anders|anderszins|beetje|behalve|behoudens|beide|beiden|ben|beneden|bent|bepaald|betreffende|bij|bijna|bijv|binnen|binnenin|blijkbaar|blijken|boven|bovenal|bovendien|bovengenoemd|bovenstaand|bovenvermeld|buiten|bv|daar|daardoor|daarheen|daarin|daarna|daarnet|daarom|daarop|daaruit|daarvanlangs|dan|dat|de|deden|deed|der|derde|derhalve|dertig|deze|dhr|die|dikwijls|dit|doch|doe|doen|doet|door|doorgaand|drie|duizend|dus|echter|een|eens|eer|eerdat|eerder|eerlang|eerst|eerste|eigen|eigenlijk|elk|elke|en|enig|enige|enigszins|enkel|er|erdoor|erg|ergens|etc|etcetera|even|eveneens|evenwel|gauw|ge|gedurende|geen|gehad|gekund|geleden|gelijk|gemoeten|gemogen|genoeg|geweest|gewoon|gewoonweg|haar|haarzelf|had|hadden|hare|heb|hebben|hebt|hedden|heeft|heel|hem|hemzelf|hen|het|hetzelfde|hier|hierbeneden|hierboven|hierin|hierna|hierom|hij|hijzelf|hoe|hoewel|honderd|hun|hunne|ieder|iedere|iedereen|iemand|iets|ik|ikzelf|in|inderdaad|inmiddels|intussen|inzake|is|ja|je|jezelf|jij|jijzelf|jou|jouw|jouwe|juist|jullie|kan|klaar|kon|konden|krachtens|kun|kunnen|kunt|laatst|later|liever|lijken|lijkt|maak|maakt|maakte|maakten|maar|mag|maken|me|meer|meest|meestal|men|met|mevr|mezelf|mij|mijn|mijnent|mijner|mijzelf|minder|miss|misschien|missen|mits|mocht|mochten|moest|moesten|moet|moeten|mogen|mr|mrs|mw|na|naar|nadat|nam|namelijk|nee|neem|negen|nemen|nergens|net|niemand|niet|niets|niks|noch|nochtans|nog|nogal|nooit|nu|nv|of|ofschoon|om|omdat|omhoog|omlaag|omstreeks|omtrent|omver|ondanks|onder|ondertussen|ongeveer|ons|onszelf|onze|onzeker|ooit|ook|op|opnieuw|opzij|over|overal|overeind|overige|overigens|paar|pas|per|precies|recent|redelijk|reeds|rond|rondom|samen|sedert|sinds|sindsdien|slechts|sommige|spoedig|steeds|tamelijk|te|tegen|tegenover|tenzij|terwijl|thans|tien|tiende|tijdens|tja|toch|toe|toen|toenmaals|toenmalig|tot|totdat|tussen|twee|tweede|u|uit|uitgezonderd|uw|vaak|vaakwat|van|vanaf|vandaan|vanuit|vanwege|veel|veeleer|veertig|verder|verscheidene|verschillende|vervolgens|via|vier|vierde|vijf|vijfde|vijftig|vol|volgend|volgens|voor|vooraf|vooral|vooralsnog|voorbij|voordat|voordezen|voordien|voorheen|voorop|voorts|vooruit|vrij|vroeg|waar|waarom|waarschijnlijk|wanneer|want|waren|was|wat|we|wederom|weer|weg|wegens|weinig|wel|weldra|welk|welke|werd|werden|werder|wezen|whatever|wie|wiens|wier|wij|wijzelf|wil|wilden|willen|word|worden|wordt|zal|ze|zei|zeker|zelf|zelfde|zelfs|zes|zeven|zich|zichzelf|zij|zijn|zijne|zijzelf|zo|zoals|zodat|zodra|zonder|zou|zouden|zowat|zulk|zulke|zullen|zult)\\z")

	txtSlice := strings.Split(txt, " ")
	for _, word := range txtSlice {
		// Skipp the stop words
		if true == re4.Match([]byte(word)) {
			continue
		}

		// checking if word is indexed
		hasEntry := false
		_, ok := ji[word]
		if !ok {
			ji[word] = []*Journal{j}
			hasEntry = true
		}
		if hasEntry {
			continue
		}

		for _, entry := range ji[word] {
			if entry == j {
				hasEntry = true
			}
		}

		if hasEntry == false {
			ji[word] = append(ji[word], j)
		}
	}
	return ji
}

// Journal structure
type Journal struct {
	Name    string
	Section string
	fs      filesystem.Filesystem
}

// JournalFactory can create journals
type JournalFactory struct {
	fileSystem filesystem.Filesystem
}

// NewJournalFactory creates a journal factory
func NewJournalFactory(fs filesystem.Filesystem) *JournalFactory {
	return &JournalFactory{fs}
}

// BuildNow builds a journal with date now
func (jf *JournalFactory) BuildNow(section string) *Journal {
	return &Journal{
		Name:    "Journal" + time.Now().Format("20060102") + ".md",
		Section: section,
		fs:      jf.fileSystem,
	}
}

// BuildDate builds a journal with a given date
// Date should be given as yyyymmdd
func (jf *JournalFactory) BuildDate(date string, section string) (*Journal, error) {
	r := regexp.MustCompile("^20[0-9]{2}[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{1}$")
	if r.Match([]byte(date)) == false {
		return nil, fmt.Errorf("Invalide date: %s", date)
	}
	return &Journal{
		Name:    "Journal" + date + ".md",
		Section: section,
		fs:      jf.fileSystem,
	}, nil
}

// BuildFromFileName builds a journal from a file name
func (jf *JournalFactory) BuildFromFileName(filename string, section string) (*Journal, error) {
	//Journal20191017.md
	r := regexp.MustCompile("^Journal20[0-9]{2}[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{1}\\.md")
	if r.Match([]byte(filename)) == false {
		return nil, fmt.Errorf("File is not a journal: %s", filename)
	}

	return &Journal{
		Name:    filename,
		Section: section,
		fs:      jf.fileSystem,
	}, nil
}

// ShortName returns the date part of the journal.Name
func (j Journal) ShortName() string {
	r := regexp.MustCompile("[0-9]{8}")
	return string(r.Find([]byte(j.Name)))
}

// ShortNameFormated makes a pritty date from the shortname of the journal
func (j Journal) ShortNameFormated() string {
	layout := "Mon 02 Jan 2006"
	date, _ := time.Parse("20060102", j.ShortName())
	return date.Format(layout)
}

// String returns the content of the journal
func (j Journal) String() string {
	b := j.fs.Read(j.Section + "/" + j.Name)
	return string(b)
}

// Save saves the content to the journal file
func (j Journal) Save(content string) {

	c := strings.Replace(content, "@t", "## "+time.Now().Format("15:04:05")+"\n", -1)
	// This spacing below is functional
	//c := fmt.Sprintf("\n\n## %s\n%s", time.Now().Format("15:04:05"), content)
	j.fs.Write(j.Section+"/"+j.Name, []byte(c))
}

// RenderToMarkDown renders the content of the journal as markdown
func (j Journal) RenderToMarkDown() string {
	md := markdown.New(markdown.XHTMLOutput(true), markdown.Breaks(true))
	return md.RenderToString([]byte(j.String()))
}

// JournalList is a sorted list of journals
type JournalList []*Journal

// BuildJournalList Creates a sorted list of journals
func (jf *JournalFactory) BuildJournalList(section string) (JournalList, error) {
	files := jf.fileSystem.Files(section)

	sort.Slice(files, func(i, j int) bool {
		jI, err := jf.BuildFromFileName(files[i], section)
		if err != nil {
			return true
		}
		jJ, err := jf.BuildFromFileName(files[j], section)
		if err != nil {
			return true
		}
		// Casting []byte from string to int
		left, _ := strconv.Atoi(jI.ShortName())
		right, _ := strconv.Atoi(jJ.ShortName())
		return left > right
	})

	list := make([]*Journal, 0)
	for _, v := range files {
		j, err := jf.BuildFromFileName(v, section)
		if err != nil {
			continue
		}
		list = append(list, j)
	}
	return list, nil
}

// CreateJournalIndex makes a index of the journals in the list
func (l *JournalList) CreateJournalIndex() JournalIndex {
	return l.createJournalIndex()
}

func (l *JournalList) createJournalIndex() JournalIndex {
	ji := JournalIndex{}
	for _, j := range *l {
		ji.add(j)
	}
	return ji
}

// GetLastJournal gets the last journal from the list
func (l *JournalList) GetLastJournal() (*Journal, error) {
	return l.getLastJournal()
}

func (l *JournalList) getLastJournal() (*Journal, error) {
	for _, journal := range *l {
		return journal, nil
	}
	return nil, fmt.Errorf("No journal found")
}

// GetPreviousNextJournal get's the previous or next journal, relative to the given journal name
// when next == false will return previous
// when next == true will return the next
func (l *JournalList) GetPreviousNextJournal(fromJournal *Journal, next bool) (*Journal, error) {
	return l.getPreviousNextJournal(fromJournal, next)
}

func (l JournalList) getPreviousNextJournal(fromJournal *Journal, next bool) (*Journal, error) {
	if fromJournal == nil {
		return nil, fmt.Errorf("FromJournal can not be nil")
	}
	elements := len(l) - 1
	for i, journal := range l {
		// We found the from journal in the journal list
		// Now we must figure out if there is a journal before and or after the current journal
		if fromJournal.ShortName() == journal.ShortName() {
			if next && i > 0 {
				return l[i-1], nil
			}
			if !next && i < elements {
				return l[i+1], nil
			}
		}
	}
	if next {
		return nil, fmt.Errorf("No next journal found")
	}
	return nil, fmt.Errorf("No previous journal found")
}
