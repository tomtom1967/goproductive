package documents

import (
	"bitbucket.org/tomtom1967/goproductive/filesystem"
	"gitlab.com/golang-commonmark/markdown"
)

// Todo reprecents the to do list
type Todo struct {
	content    []byte
	filesystem filesystem.Filesystem
	section    string
}

// TodoFactory can create journals
type TodoFactory struct {
	fileSystem filesystem.Filesystem
}

// NewTodoFactory creates a journal factory
func NewTodoFactory(fs filesystem.Filesystem) *TodoFactory {
	return &TodoFactory{fs}
}

// BuildTodo builds a new todo struct ready for action
func (tf *TodoFactory) BuildTodo(section string) *Todo {
	return tf.New(section)
}

// New returns a pointer to a Todo struct ready for action
func (tf *TodoFactory) New(section string) *Todo {
	return &Todo{
		section:    section,
		filesystem: tf.fileSystem,
	}
}

// Byte returns the content of the todo as []byte
func (td *Todo) Byte() []byte {
	if len(td.content) == 0 {
		td.content = td.filesystem.Read(td.section + "/Todo.md")
	}
	return td.content
}

// String returns the todo as a string
func (td *Todo) String() string {
	return string(td.Byte())
}

// RenderToHTML renders todo's markdown as HTML
func (td *Todo) RenderToHTML() string {
	return markdown.New(markdown.XHTMLOutput(true), markdown.Breaks(true)).RenderToString(td.Byte())
}

// Save saves the contant to the file system
func (td *Todo) Save(content []byte) {
	td.content = content
	td.filesystem.Write(td.section+"/Todo.md", td.content)
}
