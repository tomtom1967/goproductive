# Go Productive
Is a work in progress.

Go Productive is a simple web interface to create, view and update a local file's. It supports three types: **Journals**, **Todo** and **Url's**
I use the journal editor to keep track of what I'm working on during the day. The journals are saved in local text files that can easily be viewed in a simple text editor.
I use the url editer as my bookmarker tool.
I use the todo file to remind me of things  I need to do. (I use these the most)
Journals and url's are indexed and can be searched for.

# Journal and Todo's
Journals and Todo's are writen in [Markdown](https://en.wikipedia.org/wiki/Markdown). And the files are storred localy. If you want to use them on multiple machines you need some synchronisation mechanism. I use Resilio Sync for this
But I made the "filesystem" replacable. I also made a encrypted version. When starting the service from the command line you have to in put the file system you want to use. Currently this can be **memory**, **local** and **local-encrypted**

# Sections
You can segmentate your points of intrest in sections. The aim is to improve your focus. 
At the base there is always te main section. Sections you could add are *work* or *sport* or *whatever* 

# Usage
* Checkout the repo.
* Install packer 2: ▶ go get -u github.com/gobuffalo/packr/v2/...
* **Build**: ▶ $GOPATH/bin/packr2 build -v --race                
* **Run**: ./goproductive [filesystem] [-p path to where you want your files storred]
* **Example**: ▶ ./goproductive local -p ../../UrlAndNotes &;disown
* **Go to**: 127.0.0.1:9999 and start writing journals, todo's and adding URL's. 

# ToDo
1. Make it possible to add **subject files**. I type my notes always in markdown. It is an easy way to add some structure to my notes. I would like to be able to add them to go productive, and make them searcheble. 
1. Make a make file to make the build, and run steps easyer
1. I want to add a sort of web-push notifyer. This will make it possible to:  
  1.1 intergrate pommedories.  
  1.2 Add notifyers for weeakly repeating tasks
2. Make a filesystem for tardigrade to store files in the block chain

# Bug
1. Adding a section in full caps adds the section, but the section an not be reached
2. Adding a url gives a Race warning

