package main

import (
	"bitbucket.org/tomtom1967/goproductive/server"
)

func main() {
	server.Execute()
}
