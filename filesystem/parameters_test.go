package filesystem

import (
	"github.com/integrii/flaggy"
	"os"
	"testing"
)

func TestInit(t *testing.T) {
	os.Args = make([]string, 4)
	os.Args[0] = "Testing"
	os.Args[1] = "local"
	os.Args[2] = "-p"
	os.Args[3] = "../bla"

	flaggy.ResetParser()
	InitParameters()

	if Port != "9999" {
		t.Errorf("Wanted port 9999 got %s", Port)
	}
	if Parameters["localEncrypted"].Used() == true {
		t.Errorf("Wanted false got: true")
	}
	if Parameters["local"].Used() == false {
		t.Errorf("Wanted true got: false")
	}
	if Parameters["local"].Parameters() == nil {
		t.Errorf("Wanted %#v, got %#v", map[string]string{"path": "../bla"}, Parameters["local"].Parameters())
	}
	if Parameters["localEncrypted"].Parameters() != nil {
		t.Errorf("Wanted nil, got %#v", Parameters["localEncrypted"].Parameters())
	}
	if *Parameters["local"].params["path"] != "../bla" {
		t.Errorf(`Wanted: "../bla", got %s`, *Parameters["local"].params["path"])
	}
}
