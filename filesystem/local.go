package filesystem

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

// Local interfaces with the local file system
type Local struct {
	path string
}

func (ff *Factory) buildLocal(settings map[string]string) (Filesystem, error) {
	if path, ok := settings["path"]; ok {
		if _, err := os.Stat(path); err != nil {
			if os.IsNotExist(err) {
				if err := os.MkdirAll(path, 0700); err != nil {
					return nil, err
				}
				return &Local{path}, nil
			}
		}
		return &Local{path}, nil
	}
	return nil, fmt.Errorf("Unknown error building Local")
}

// NewLocal makes a new Local
func NewLocal(settings map[string]string) (*Local, error) {
	if path, ok := settings["path"]; ok {
		return &Local{path}, nil
	}
	return nil, fmt.Errorf("Setting path not found")
}

func (l *Local) Read(filename string) []byte {
	b, err := ioutil.ReadFile(l.path + "/" + filename)
	if _, ok := err.(*os.PathError); ok {
		return []byte{}
	}
	if err != nil {
		panic(err)
	}

	return b
}

func (l *Local) Write(filename string, content []byte) {
	err := ioutil.WriteFile(l.path+"/"+filename, content, 0644)
	if err != nil {
		panic(err)
	}
}

// Sections gets the list of created sections.
func (l *Local) Sections() []string {
	files, err := ioutil.ReadDir(l.path)
	if err != nil {
		log.Fatal(err)
	}

	sections := []string{}
	for _, file := range files {
		if file.IsDir() {
			sections = append(sections, file.Name())
		}
	}
	return sections
}

// AddSection adds a section to the project
func (l *Local) AddSection(section string) {
	p := filepath.Join(l.path, section)

	if _, err := os.Stat(p); os.IsNotExist(err) {
		os.MkdirAll(p, os.ModePerm)
	}
}

// Files returns a list of files in the section
func (l *Local) Files(section string) []string {
	files, err := ioutil.ReadDir(filepath.Join(l.path, section))
	if err != nil {
		panic(err)
	}

	list := []string{}
	for _, file := range files {
		if file.IsDir() == false {
			list = append(list, file.Name())
		}
	}
	return list
}
