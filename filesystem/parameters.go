package filesystem

import (
	"github.com/integrii/flaggy"
)

// CommandParameters is a grouping of a subcommand and its parameters
type CommandParameters struct {
	command *flaggy.Subcommand
	params  map[string]*string
}

// Used checks if a command is used
func (cp *CommandParameters) Used() bool {
	return cp.command.Used
}

// Parameters returns the parameters that belong to a command
func (cp *CommandParameters) Parameters() map[string]string {
	if cp.command.Used {
		r := make(map[string]string)
		for k, v := range cp.params {
			r[k] = *v
		}
		return r
	}
	return nil
}

// CommandParameterList is a list of command parameters
type CommandParameterList map[string]*CommandParameters

// Port the http lisening port
var Port = "9999"

// Parameters is the list of command parameters
var Parameters = CommandParameterList{
	"local":          &CommandParameters{nil, map[string]*string{"path": new(string)}},
	"localEncrypted": &CommandParameters{nil, map[string]*string{"path": new(string), "password": new(string)}},
	"memory":         &CommandParameters{nil, nil},
}

// InitParameters makes te sub-commands and parameters and parses the command line input
func InitParameters() {
	flaggy.SetName("Go productive")
	flaggy.SetDescription("A web service to keep personal notes, journals and bookmarks")
	flaggy.String(&Port, "po", "port", "The http listening port")

	Parameters["local"].command = flaggy.NewSubcommand("local")
	Parameters["local"].command.Description = "Wil save the files on the local filesystem"
	Parameters["local"].command.String(Parameters["local"].params["path"], "p", "path", "The path where the files will be saved")
	flaggy.AttachSubcommand(Parameters["local"].command, 1)

	Parameters["localEncrypted"].command = flaggy.NewSubcommand("localEncrypted")
	Parameters["localEncrypted"].command.Description = "Will save the files encrypted to the local filesystem"
	Parameters["localEncrypted"].command.String(Parameters["localEncrypted"].params["path"], "p", "path", "The path where the files will be saved")
	Parameters["localEncrypted"].command.String(Parameters["localEncrypted"].params["password"], "pw", "password", "The 32 character password to encrypt the files")
	flaggy.AttachSubcommand(Parameters["localEncrypted"].command, 1)

	Parameters["memory"].command = flaggy.NewSubcommand("memmory")
	Parameters["memory"].command.Description = "Will save the files in memory (After restarting all files are gone)"

	flaggy.Parse()

}
