package filesystem

import (
	"crypto/sha256"
	"github.com/integrii/flaggy"
	"os"
	"path/filepath"
	"testing"
)

func testDir() string {
	dir, _ := filepath.Abs("./")
	return dir + "/testData"
}

func TestWriteRead(t *testing.T) {
	//fs, err := new(FilesystemFactory).buildFsLocal(map[string]string{"path": testDir()})
	fs, err := new(Factory).BuildMemory(nil)
	if err != nil {
		panic(err)
	}
	filename := "main/tgtTest.txt"
	want := []byte("Hello World")

	fs.AddSection("main")
	fs.Write(filename, want)
	got := string(fs.Read(filename))
	if got != string(want) {
		t.Errorf("Unexpected content, want %s, got %s\n", want, got)
	}
}

func TestWriteReadEncrypted(t *testing.T) {
	os.Args = make([]string, 5)

	os.Args[0] = "test"
	os.Args[1] = "localEncrypted"
	os.Args[2] = "-p"
	os.Args[3] = testDir() + "/crypto"
	os.Args[4] = "-pw=eenlangpassword"

	flaggy.ResetParser()
	InitParameters()

	fs, err := new(Factory).Build(Parameters)
	if err != nil {
		panic(err)
	}
	filename := "main/tgtTest.txt"
	want := []byte("Hello World")

	fs.AddSection("main")
	fs.Write(filename, want)
	got := string(fs.Read(filename))
	if got != string(want) {
		t.Errorf("Unexpected content, want %s, got %s\n", want, got)
	}
}

func TestFilenameEncryption(t *testing.T) {
	ff := new(Factory)
	local, err := ff.BuildMemory(map[string]string{})
	if err != nil {
		panic(err)
	}
	pwHash := sha256.Sum256([]byte("ditiseenwachtwoord"))
	fsCrypto := &Encrypted{local, pwHash[:], map[string]string{}}

	testString := "Bla/TgtTest.tgt"
	encrypted := fsCrypto.encryptFilename(testString)

	decrypted := fsCrypto.decryptFilename(encrypted)

	if decrypted != testString {
		t.Errorf("wanted %s, got %s\n", testString, decrypted)
	}
}

func TestFsEncryptionInitCach(t *testing.T) {
	ff := new(Factory)

	local, err := ff.BuildMemory(map[string]string{})
	if err != nil {
		panic(err)
	}
	pwHash := sha256.Sum256([]byte("ditiseenwachtwoord"))
	fsCrypto := &Encrypted{local, pwHash[:], map[string]string{}}
	fsCrypto.initFileCache()

	fsCrypto.AddSection("main")
	fsCrypto.Write(filepath.Join("main", "tgtTest.tgt"), []byte("Hello World"))
	if len(local.Sections()) > 1 {
		t.Errorf("")
	}
}
