package filesystem

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"path/filepath"
	"strings"
)

// Encrypted is a decorator that takes care of encryption
type Encrypted struct {
	fileSystem   Filesystem
	passwordHash []byte
	filenameCash map[string]string
}

func (ff *Factory) buildEncrypted(settings map[string]string) (Filesystem, error) {
	var pwHash [32]byte

	local, err := ff.buildLocal(settings)
	if err != nil {
		return nil, err
	}

	if pw, ok := settings["password"]; ok {
		pwHash = sha256.Sum256([]byte(pw))
		fs := &Encrypted{
			fileSystem:   local,
			passwordHash: pwHash[:],
			filenameCash: map[string]string{},
		}
		fs.initFileCache()
		return fs, nil
	}
	return nil, fmt.Errorf("Unknown error building FsEncrypted")
}

func (le *Encrypted) Read(filename string) []byte {
	if encrypted, ok := le.filenameCash[filename]; ok == true {
		return le.decrypt(le.fileSystem.Read(encrypted))
	}
	return []byte{}
}

func (le *Encrypted) Write(filename string, content []byte) {
	if _, ok := le.filenameCash[filename]; ok == false {
		le.filenameCash[filename] = le.encryptFilename(filename)
	}
	le.fileSystem.Write(le.filenameCash[filename], le.encrypt(content))
}

// Sections returns a list of the sections
func (le *Encrypted) Sections() []string {
	var decrypted []string
	encrypted := le.fileSystem.Sections()

	for _, section := range encrypted {
		decrypted = append(decrypted, le.decryptFilename(section))
	}
	return decrypted
}

// AddSection Adds a section to the project
func (le *Encrypted) AddSection(section string) {
	if _, ok := le.filenameCash[section]; ok == false {
		le.filenameCash[section] = le.encryptFilename(section)
		le.fileSystem.AddSection(le.filenameCash[section])
	}
	//	Don't add the section if it exist in filenameCache
}

// Files gets a list of files from a section
func (le *Encrypted) Files(section string) []string {
	encrypted := le.fileSystem.Files(section)
	decrypted := make([]string, len(encrypted))
	for k, file := range encrypted {
		decrypted[k] = le.decryptFilename(file)
	}
	return decrypted
}

// Every time you encrypt the same string, the result will be different. So encrypting a file name, and check if it exists, will allways give a negative result.
// What I do here is scan all the encrypted files and decrypt them, and save de decrypted filename and the current encrypted filename into a cache.
// This way it is easy to check if a file already exist.

// initFileCach filles the file cache, connecting the decrypted filename to the current encrypted filename
func (le *Encrypted) initFileCache() {
	for _, section := range le.fileSystem.Sections() {
		// Check if section already exists
		if _, ok := le.filenameCash[le.decryptFilename(section)]; ok {
			panic(fmt.Errorf("Multiple encrypted sections found for %s,", le.decryptFilename(section)))
		} else {
			le.filenameCash[le.decryptFilename(section)] = section
		}
		for _, file := range le.fileSystem.Files(section) {
			if section != "" {
				le.filenameCash[filepath.Join(le.decryptFilename(section), le.decryptFilename(file))] = filepath.Join(section, file)
			} else {
				le.filenameCash[le.decryptFilename(file)] = file
			}
		}
	}
}

func (le *Encrypted) encrypt(plainData []byte) []byte {
	c, err := aes.NewCipher(le.passwordHash)
	if err != nil {
		panic(err)
	}

	// gcm or Galois/Counter Mode, is a mode of operation
	// for symmetric key cryptographic block ciphers
	// - https://en.wikipedia.org/wiki/Galois/Counter_Mode
	gcm, err := cipher.NewGCM(c)
	if err != nil {
		panic(err)
	}

	// creates a new byte array the size of the nonce
	// which must be passed to Seal
	nonce := make([]byte, gcm.NonceSize())
	// populates our nonce with a cryptographically secure
	// random sequence
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		panic(err)
	}

	// here we encrypt our text using the Seal function
	// Seal encrypts and authenticates plaintext, authenticates the
	// additional data and appends the result to dst, returning the updated
	// slice. The nonce must be NonceSize() bytes long and unique for all
	// time, for a given key.
	return gcm.Seal(nonce, nonce, plainData, nil)
}

func (le *Encrypted) decrypt(ciphertext []byte) []byte {
	c, err := aes.NewCipher(le.passwordHash)
	if err != nil {
		panic(err)
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		panic(err)
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		panic(err)
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		panic(err)
	}
	return plaintext
}

// encryptFilename makes sure that file names are eccrypted only once.
func (le *Encrypted) encryptFilename(filename string) string {

	parts := strings.Split(filename, "/")
	switch len(parts) {
	case 1:
		if cyvered, ok := le.filenameCash[filename]; ok {
			return cyvered
		}
		return le.encodeB64(le.encrypt([]byte(parts[0])))
	case 2:
		cyverdSection, ok := le.filenameCash[parts[0]]
		if ok == false {
			cyverdSection = le.encodeB64(le.encrypt([]byte(parts[0])))
		}
		cyverdFile, ok := le.filenameCash[parts[1]]
		if ok == false {
			cyverdFile = le.encodeB64(le.encrypt([]byte(parts[1])))
		}
		return fmt.Sprintf("%s/%s", cyverdSection, cyverdFile)
	default:
		panic(fmt.Errorf("Unsupported number of parts(%d) in filename %s", len(parts), filename))
	}

}

func (le *Encrypted) decryptFilename(filename string) string {
	if filename == "" {
		return ""
	}
	parts := strings.Split(filename, "/")
	partsDecoded := make([][]byte, len(parts))
	for k, v := range parts {
		decoV, err := le.decodeB64(v)
		if err != nil {
			panic(err)
		}
		partsDecoded[k] = decoV
	}

	if len(partsDecoded) == 1 {
		return string(le.decrypt(partsDecoded[0]))
	}
	if len(parts) > 2 {
		panic(fmt.Errorf("to many parts in filename"))
	}

	return fmt.Sprintf("%s/%s",
		le.decrypt(partsDecoded[0]),
		le.decrypt(partsDecoded[1]))
}

func (le *Encrypted) encodeB64(plain []byte) string {
	return strings.Replace(base64.StdEncoding.EncodeToString(plain), "/", "-", -1)
}
func (le *Encrypted) decodeB64(encoded string) ([]byte, error) {
	return base64.StdEncoding.DecodeString(strings.Replace(encoded, "-", "/", -1))
}
