package filesystem

import (
	"os"

	"github.com/spf13/afero"
)

// Memory saves files in memory
// NOTE: After restart all saved files will be gone
// This file system is mainly used for testing
type Memory struct {
	afero afero.Fs
}

// BuildMemory returns a pointer to a memory filesystem
func (ff *Factory) BuildMemory(settings map[string]string) (Filesystem, error) {
	return &Memory{afero.NewMemMapFs()}, nil
}

func (m *Memory) Read(filename string) []byte {
	content, err := afero.ReadFile(m.afero, filename)
	if _, ok := err.(*os.PathError); ok {
		return []byte{}
	}

	if err != nil {
		panic(err)
	}
	return content
}

func (m *Memory) Write(filename string, content []byte) {
	err := afero.WriteFile(m.afero, filename, content, 0644)
	if err != nil {
		panic(err)
	}
}

// Sections gets the list of created sections.
func (m *Memory) Sections() []string {
	files, err := afero.ReadDir(m.afero, "./")
	if err != nil {
		panic(err)
	}

	sections := []string{}
	for _, file := range files {
		if file.IsDir() {
			sections = append(sections, file.Name())
		}
	}
	return sections
}

// AddSection adds a section to the project
func (m *Memory) AddSection(section string) {
	exists, err := afero.DirExists(m.afero, section)
	if err != nil {
		panic(err)
	}
	if exists == false {
		err := m.afero.MkdirAll(section, os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
}

// Files returns a list of files in the section
func (m *Memory) Files(section string) []string {
	exists, err := afero.DirExists(m.afero, section)
	if err != nil {
		panic(err)
	}
	if exists == false {
		return []string{}
	}

	files, err := afero.ReadDir(m.afero, section)
	if err != nil {
		panic(err)
	}

	list := []string{}
	for _, file := range files {
		if file.IsDir() == false {
			list = append(list, file.Name())
		}
	}
	return list
}
