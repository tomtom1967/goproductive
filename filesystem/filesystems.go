package filesystem

// Filesystem to be used as a abstract file system
type Filesystem interface {
	AddSection(string)
	Files(string) []string
	Read(string) []byte
	Sections() []string
	Write(string, []byte)
}

// Factory creates diverse Filsystem's
type Factory struct{}

// NewFactory returns a pointer to a Factory ready for action
func NewFactory() *Factory {
	return &Factory{}
}

// Build is the builder function of the factory that will returns a FileSystem or a Error
func (ff *Factory) Build(l CommandParameterList) (Filesystem, error) {

	for k, v := range l {
		switch k {
		case "local":
			if v.Used() {
				return ff.buildLocal(v.Parameters())
			}
		case "localEncrypted":
			if v.Used() {
				// Should ask for password here.
				return ff.buildEncrypted(v.Parameters())
			}
		case "memory":
			if v.Used() {
				return ff.BuildMemory(v.Parameters())
			}
		}
	}
	return ff.BuildMemory(nil)
}

// MakeFileSystem makes a filesystem using the parameters, default is memory fs
func MakeFileSystem() (Filesystem, error) {
	return NewFactory().Build(Parameters)
}
