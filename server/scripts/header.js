window.onload = function() {
    applyDefaultTableStyle();
};

function applyDefaultTableStyle() {
    var tables = document.getElementsByTagName("table")
    for (let item of tables) {
      item.className += " w3-table w3-striped w3-bordered"
    }
}