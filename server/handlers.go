package server

import (
	"embed"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"
	"sync"

	HTMLtpl "html/template"
	TXTtpl "text/template"

	"time"

	"github.com/gorilla/mux"

	"bitbucket.org/tomtom1967/goproductive/documents"
	"bitbucket.org/tomtom1967/goproductive/filesystem"
)

var (
	router  *mux.Router
	jiMutex = sync.Mutex{}

	journalIndexes = map[string]documents.JournalIndex{}

	//go:embed templates images scripts data
	staticEmbed embed.FS

	fs       filesystem.Filesystem
	journalF *documents.JournalFactory
	todoF    *documents.TodoFactory
	urlF     *documents.URLFactory
)

const (
	tplIndex             = "templates/index.html"
	tplHeader            = "templates/header.html"
	tplFooter            = "templates/footer.html"
	tplTodoHeadLess      = "templates/todoHeadLess.html"
	tplJournalMDHeadLess = "templates/journalMdHeadLess.html"
	tplJournalTemplate   = "templates/journalTemplate.html"
	tplTodoForm          = "templates/todoForm.html"
	tplURLs              = "templates/urls.html"
	tplSearch            = "templates/search.html"
	tplJournalForm       = "templates/journalForm.html"
	tplJournalMD         = "templates/journalMd.html"
	tplSectionForm       = "templates/sectionForm.html"

	errorExecute = "Error execute: %s"
	errorPosting = "Error posting: %s"
)

func init() {
	var err error
	filesystem.InitParameters()

	fs, err = filesystem.MakeFileSystem()
	if err != nil {
		panic(err)
	}

	// When started with empty dir, at leaset the main section folder should be available
	if len(fs.Sections()) == 0 {
		fs.AddSection("main")
	}

	journalF = documents.NewJournalFactory(fs)
	todoF = documents.NewTodoFactory(fs)
	urlF = documents.NewURLFactory(fs)

	indexJournals(fs.Sections())
}

func indexJournals(sections []string) {
	// Filling the journal indexes for every section
	// This makes searching faster.
	// When adding a new journal, the journals will be re-indexed
	ch := make(chan map[string]documents.JournalIndex, len(fs.Sections()))
	for _, s := range fs.Sections() {
		go func(section string) {
			jl, err := journalF.BuildJournalList(section)
			if err != nil {
				panic(err)
			}
			ch <- map[string]documents.JournalIndex{section: jl.CreateJournalIndex()}
		}(s)
	}
	// Adding the created indexes to the index variable
	loop := 0
	jiMutex.Lock()
	for m := range ch {
		loop++
		for k, v := range m {
			journalIndexes[k] = v
		}
		if loop == len(fs.Sections()) {
			close(ch)
		}
	}
	jiMutex.Unlock()
}

// Execute starts the webserver to edit the local files
func Execute() {

	router = mux.NewRouter()

	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.FS(staticEmbed))))

	router.HandleFunc("/section", SectionHandler).Methods(http.MethodGet).Name("section")
	router.HandleFunc("/section", SectionPostHandler).Methods(http.MethodPost).Name("section")

	router.HandleFunc("/{section:[a-zA-Z]*}", HomeHandler).Methods(http.MethodGet).Name("home")

	// Add URLS Get handler -->
	router.HandleFunc("/{section:[a-zA-Z]*}/urls", URLHandler).Methods(http.MethodGet).Name("url")
	router.HandleFunc("/{section:[a-zA-Z]*}/urls", URLAddHandler).Methods(http.MethodPost).Name("urlAdd")
	router.HandleFunc("/{section:[a-zA-Z]*}/urls/delete/{hash:.+}", URLDeleteHandler).Methods(http.MethodGet).Name("urlDelete")

	router.HandleFunc("/{section:[a-zA-Z]*}/searchp", SearchPadgeHandler).Methods(http.MethodGet, http.MethodPost).Name("searchPadge")

	// This search url is used for clicking om url tags. Clicking on a tag you do a search
	router.HandleFunc("/{section:[a-zA-Z]*}/search/{searchword:.*}", SearchHandler).Methods(http.MethodGet).Name("searchGet")
	router.HandleFunc("/{section:[a-zA-Z]*}/search", SearchHandler).Methods(http.MethodPost).Name("searchPost")

	router.HandleFunc("/{section:[a-zA-Z]*}/journal", JournalHandler).Methods(http.MethodGet).Name("journalCreate")
	router.HandleFunc("/{section:[a-zA-Z]*}/journal", JournalPostHandler).Methods(http.MethodPost).Name("journalPost")
	router.HandleFunc("/{section:[a-zA-Z]*}/journal/{id:[0-9]+}", JournalViewHandler).Methods(http.MethodGet).Name("journalGet")
	router.HandleFunc("/{section:[a-zA-Z]*}/journalheadless/{id:[0-9]+}", JournalHeadLessViewHandler).Methods(http.MethodGet).Name("journalGetHeadLess")

	router.HandleFunc("/{section:[a-zA-Z]*}/todo", TodoHandler).Methods(http.MethodGet).Name("todo")
	router.HandleFunc("/{section:[a-zA-Z]*}/todo", TodoPostHandler).Methods(http.MethodPost).Name("todoPost")

	srv := &http.Server{
		Handler: router,
		Addr:    "127.0.0.1:9999",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}

// HomeHandler shows all the urls
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	tpl, err := TXTtpl.ParseFS(staticEmbed, tplIndex)
	tpl.ParseFS(staticEmbed, tplHeader)
	tpl.ParseFS(staticEmbed, tplTodoHeadLess)
	tpl.ParseFS(staticEmbed, tplJournalMDHeadLess)
	tpl.ParseFS(staticEmbed, tplFooter)

	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}

	jl, _ := journalF.BuildJournalList(s)
	lastJournal, _ := jl.GetLastJournal()
	olderJournal := &documents.Journal{}
	newerJournal := &documents.Journal{}
	if lastJournal != nil {
		olderJournal, _ = jl.GetPreviousNextJournal(lastJournal, false)
		newerJournal, _ = jl.GetPreviousNextJournal(lastJournal, true)
	}

	data := struct {
		ListUrls     *documents.URLlist
		Journal      *documents.Journal
		OlderJournal *documents.Journal
		NewerJournal *documents.Journal
		ToDo         *documents.Todo
		Section      string
		Sections     []string
	}{
		urlF.BuildList(s),
		lastJournal,
		olderJournal,
		newerJournal,
		todoF.BuildTodo(s),
		s,
		fs.Sections(),
	}

	err = tpl.Execute(w, data)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}
}

// URLHandler displays the url's
func URLHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	urls := urlF.BuildList(s)

	tpl, err := TXTtpl.ParseFS(staticEmbed, tplURLs)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
		return
	}
	tpl.ParseFS(staticEmbed, tplHeader)
	tpl.ParseFS(staticEmbed, tplFooter)

	data := struct {
		ListUrls *documents.URLlist
		Section  string
		Sections []string
	}{
		urls,
		s,
		fs.Sections(),
	}
	err = tpl.Execute(w, data)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}
}

// URLAddHandler adds a link and tags to the url list
func URLAddHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	err := r.ParseForm()
	if err != nil {
		fmt.Fprintf(w, errorPosting, err)
	}
	urls := urlF.BuildList(s)

	re := regexp.MustCompile("\\s+")
	tags := re.ReplaceAll([]byte(r.FormValue("tags")), []byte(","))

	urls.Add(urlF.BuildURL(fmt.Sprintf("%s::%s\n", strings.ToLower(string(tags)), r.FormValue("link"))))
	urls.Save()
	http.Redirect(w, r, "/"+s, http.StatusFound)
}

// URLDeleteHandler is taking care of removing a bookmark from the list
func URLDeleteHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	vars := mux.Vars(r)
	hash := vars["hash"]

	list := urlF.BuildList(s)
	list.Delete(hash)
	http.Redirect(w, r, "/"+s, http.StatusFound)
}

// SearchPadgeHandler shows te search padge.
func SearchPadgeHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	err := r.ParseForm()
	if err != nil {
		fmt.Fprintf(w, errorPosting, err)
	}
	searchword := strings.ToLower(r.FormValue("searchword"))
	sections := r.Form["sections"]

	foundURLs := documents.URLlist{}
	foundJournals := documents.JournalList{}
	for _, section := range sections {
		// Searching url's
		uls := urlF.BuildList(section)
		foundURLs.List = append(foundURLs.List, uls.Search(searchword).List...)
		// Searching for Journals
		foundJournals = append(foundJournals, journalIndexes[section].FindAny(searchword)...)
	}

	// Searching for Journals
	//journalIndexes
	tpl, err := TXTtpl.ParseFS(staticEmbed, tplSearch)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
		return
	}
	tpl.ParseFS(staticEmbed, tplHeader)
	tpl.ParseFS(staticEmbed, tplFooter)

	data := struct {
		FoundURLs     documents.URLlist
		FoundJournals documents.JournalList
		Section       string
		Sections      []string
		Searchword    string
	}{
		foundURLs,
		foundJournals,
		s,
		fs.Sections(),
		searchword,
	}

	err = tpl.Execute(w, data)
	if err != nil {
		fmt.Fprintf(w, "Error Executing templates: %s", err)
	}
}

// SearchHandler searches for a keyword in the slice of url's
// @todo This one should be merged with the root controller. Thy are almost the same.
func SearchHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	vars := mux.Vars(r)
	searchword := ""
	ok := false
	if searchword, ok = vars["searchword"]; !ok {
		err := r.ParseForm()
		if err != nil {
			fmt.Fprintf(w, errorPosting, err)
		}
		searchword = r.FormValue("searchword")
	}
	found := urlF.BuildList(s).Search(strings.ToLower(searchword))

	tpl, err := TXTtpl.ParseFS(staticEmbed, tplIndex)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
		return
	}
	tpl.ParseFS(staticEmbed, tplHeader)
	tpl.ParseFS(staticEmbed, tplFooter)
	tpl.ParseFS(staticEmbed, tplJournalMDHeadLess)
	tpl.ParseFS(staticEmbed, tplTodoHeadLess)

	//jl, _ := getJournalsList(p)
	jlFound := journalIndexes[s].FindAny(searchword)
	lastJournal, _ := jlFound.GetLastJournal()
	olderJournal, _ := jlFound.GetPreviousNextJournal(lastJournal, false)
	//newerJournal, _ := jlFound.getPreviousNextJournal(lastJournal.ShortName(), true)

	data := struct {
		ListUrls     documents.URLlist
		Journal      *documents.Journal
		OlderJournal *documents.Journal
		NewerJournal documents.Journal
		ToDo         *documents.Todo
		Section      string
		Sections     []string
	}{
		found,
		lastJournal,
		olderJournal,
		documents.Journal{},
		todoF.BuildTodo(s),
		s,
		fs.Sections(),
	}

	err = tpl.Execute(w, data)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}
}

// JournalHandler creates the journals writen during the day
func JournalHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	j := journalF.BuildNow(s)
	l, err := journalF.BuildJournalList(s)
	if err != nil {
		fmt.Fprintf(w, "Error getJournalsList: %s", err)
	}

	content := struct {
		Journal  string
		List     documents.JournalList
		Section  string
		Sections []string
	}{j.String(), l, s, fs.Sections()}

	tpl, err := TXTtpl.ParseFS(staticEmbed, tplJournalForm)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}
	tpl.ParseFS(staticEmbed, tplJournalTemplate)
	tpl.ParseFS(staticEmbed, tplHeader)
	tpl.ParseFS(staticEmbed, tplFooter)

	err = tpl.Execute(w, content)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}
}

// JournalPostHandler writes the journals to file
func JournalPostHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	err := r.ParseForm()
	if err != nil {
		fmt.Fprintf(w, errorPosting, err)
	}
	if r.FormValue("journal") == "" {
		http.Redirect(w, r, s+"/journal", http.StatusFound)
		return
	}

	j := journalF.BuildNow(s)
	j.Save(r.FormValue("journal"))

	// Re-indexing the journals
	go func() {
		jl, err := journalF.BuildJournalList(s)
		if err != nil {
			panic(err)
		}
		i := jl.CreateJournalIndex()
		jiMutex.Lock()
		journalIndexes[s] = i
		jiMutex.Unlock()
	}()

	http.Redirect(w, r, "/"+s, http.StatusFound)
}

// JournalViewHandler gets a specifick journal and renders it as markdown
func JournalViewHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	vars := mux.Vars(r)
	j, err := journalF.BuildDate(vars["id"], s)
	if err != nil {
		fmt.Fprintf(w, "Error:%s", err)
	}

	jl, _ := journalF.BuildJournalList(s)
	oJ, _ := jl.GetPreviousNextJournal(j, false)
	nJ, _ := jl.GetPreviousNextJournal(j, true)

	content := struct {
		Journal      *documents.Journal
		OlderJournal *documents.Journal
		NewerJournal *documents.Journal
		Section      string
		Sections     []string
	}{j, oJ, nJ, s, fs.Sections()}

	tpl, err := TXTtpl.ParseFS(staticEmbed, tplJournalMD)
	tpl.ParseFS(staticEmbed, tplJournalMDHeadLess)
	tpl.ParseFS(staticEmbed, tplHeader)
	tpl.ParseFS(staticEmbed, tplFooter)

	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}

	err = tpl.Execute(w, content)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}
}

// JournalHeadLessViewHandler is called bij javascript
func JournalHeadLessViewHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	vars := mux.Vars(r)
	j, err := journalF.BuildDate(vars["id"], s)
	if err != nil {
		fmt.Fprintf(w, "Error:%s", err)
	}

	jl, _ := journalF.BuildJournalList(s)
	oj, _ := jl.GetPreviousNextJournal(j, false)
	nj, _ := jl.GetPreviousNextJournal(j, true)

	content := struct {
		Journal      *documents.Journal
		NewerJournal *documents.Journal
		OlderJournal *documents.Journal
		Section      string
		Sections     []string
	}{j, nj, oj, s, fs.Sections()}

	tpl, err := TXTtpl.ParseFS(staticEmbed, tplJournalMDHeadLess)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}

	err = tpl.Execute(w, content)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}
}

// TodoHandler creates the journals writen during the day
func TodoHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	content := struct {
		Todo     string
		Section  string
		Sections []string
	}{
		todoF.BuildTodo(s).String(),
		s,
		fs.Sections(),
	}

	tpl, err := HTMLtpl.ParseFS(staticEmbed, tplTodoForm)
	tpl.ParseFS(staticEmbed, tplHeader)
	tpl.ParseFS(staticEmbed, tplFooter)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}

	err = tpl.Execute(w, content)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}
}

// TodoPostHandler writes the journals to file
func TodoPostHandler(w http.ResponseWriter, r *http.Request) {
	s := getSection(w, r)
	if s == "" {
		return
	}

	err := r.ParseForm()
	if err != nil {
		fmt.Fprintf(w, errorPosting, err)
	}
	if r.FormValue("todo") == "" {
		http.Redirect(w, r, "/todo", http.StatusFound)
		return
	}

	todo := todoF.BuildTodo(s)

	// This the spacing below is functional
	content := fmt.Sprintf(`%s`, r.FormValue("todo"))
	todo.Save([]byte(content))

	http.Redirect(w, r, "/"+s, http.StatusFound)
}

// SectionHandler presents the section form
func SectionHandler(w http.ResponseWriter, r *http.Request) {
	content := struct {
		Section  string
		Sections []string
		Error    string
	}{"main", fs.Sections(), ""}

	tpl, err := HTMLtpl.ParseFS(staticEmbed, tplSectionForm) //hTemplate.New("Section")
	tpl.ParseFS(staticEmbed, tplHeader)
	tpl.ParseFS(staticEmbed, tplFooter)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}

	err = tpl.Execute(w, content)
	if err != nil {
		fmt.Fprintf(w, errorExecute, err)
	}
}

// SectionPostHandler makes te section
func SectionPostHandler(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("section") == "" {
		http.Redirect(w, r, "/section", http.StatusFound)
		return
	}
	fs.AddSection(r.FormValue("section"))
	http.Redirect(w, r, "/"+r.FormValue("section"), http.StatusFound)
}

func getSection(w http.ResponseWriter, r *http.Request) string {
	vars := mux.Vars(r)

	s := ""
	ok := false
	// No section set, redirect to main
	if s, ok = vars["section"]; ok {
		if len(s) == 0 {
			http.Redirect(w, r, "/main", http.StatusFound)
			return ""
		}
	}

	exists := false
	for _, v := range fs.Sections() {
		if strings.ToLower(s) == v {
			exists = true
		}
	}
	// Section does not exist, redirect to section
	if !exists {
		tpl, err := HTMLtpl.ParseFS(staticEmbed, tplSectionForm)
		tpl.ParseFS(staticEmbed, tplHeader)
		tpl.ParseFS(staticEmbed, tplFooter)
		if err != nil {
			fmt.Fprintf(w, errorExecute, err)
		}
		content := struct {
			Section  string
			Sections []string
			Error    string
		}{s, fs.Sections(), fmt.Sprintf("Sectie %s, niet gevonden. Maak een nieuwe sectie aan.", s)}

		tpl.Execute(w, content)
		return ""
	}

	return strings.ToLower(s)
}
