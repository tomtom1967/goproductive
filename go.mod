module bitbucket.org/tomtom1967/goproductive

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/integrii/flaggy v1.4.4
	github.com/spf13/afero v1.5.1
	gitlab.com/golang-commonmark/linkify v0.0.0-20200225224916-64bca66f6ad3 // indirect
	gitlab.com/golang-commonmark/markdown v0.0.0-20191127184510-91b5b3c99c19
	gitlab.com/opennota/wd v0.0.0-20191124020556-236695b0ea63 // indirect
	golang.org/x/text v0.3.5 // indirect
)
